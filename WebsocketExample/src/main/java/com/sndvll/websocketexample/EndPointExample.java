/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sndvll.websocketexample;

import java.io.IOException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author sndvll
 */
@ServerEndpoint("/example")
public class EndPointExample {
    
    public EndPointExample(){
        System.out.printf(this.getClass() + " laddad");
    }
    
    @OnOpen
    public void onOpen(Session session) {
        System.out.printf("Session skapad, id: %s%n", session.getId()); 
        try {
            session.getBasicRemote().sendText("Koppling lyckades!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.printf("Meddelande mottaget. Sessions id: %s. Meddelande: %s%n", session.getId(), message);
        try {
            session.getBasicRemote().sendText(String.format("Meddelande mottaget: %s%n", message));
        } catch (IOException ex) {
            ex.printStackTrace();
        }     
    }
    
    @OnError
    public void onError(Throwable e){
        e.printStackTrace();
    }
    
   @OnClose
   public void onClose(Session session) {
       System.out.printf("Session stängd, id: %s%n", session.getId());
   }
    
}
