var webSocketClient = require('websocket').client;

var client = new webSocketClient();

client.on('connectionFailed', err => {
    console.log('Connect error: ' + err.toString());
});

client.on('connect', conn => {
    console.log("WebSocket Client Connected");
    conn.on('error', err => {
        console.log("Connection Error: " + err.toString());
    });
    conn.on('close', () => {
        console.log('Connection closed');
    });
    conn.on('message', mess => {
        if(mess.type === 'utf8') {
            console.log("Server> '" + mess.utf8Data + "'");
        }
    });

    function sendMessage(){
        if(conn.connected) {
            var number = Math.round(Math.random() * 0xFFFFF);
            conn.sendUTF(number.toString())
            console.log("Klient> " + number.toString());
            setTimeout(sendMessage, 2000);
        }
    }
    sendMessage();
});

client.connect('ws://194.28.120.178:8080/WebsocketExample/example');